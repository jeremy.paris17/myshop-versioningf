# TP1 - myShop-versionningf

- <Charline Marques> <Jérémy Paris>

1. istes des étapes réalisées dans le TP
    Toutes les étapes ont été réalisées.
2. Listes des étapes non réalisées dans le TP
    Les tests n'ont pas été détaillés.
3. Répartition des taches dans le binôme
    Les tâches ont été réalisées en parallèle.
4. Liste des classes couvertes par des tests unitaires
    Aucune.
5. Tous autres commentaires que vous jugez utile sur votre TP
    Nous avons réalisé plusieurs versions de ce TP afin de rendre un travail à peu prêt présentable.
    Ce TP est éprouvant avec les bases fébriles que nous avons.
    Et souvent frustrant : au moindre bloquage on peut perdre des heures de travail.
    Impossible de commencer le TP2 et de réaliser des tests unitaires rigoureux.

