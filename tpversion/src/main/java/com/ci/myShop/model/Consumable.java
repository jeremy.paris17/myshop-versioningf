package com.ci.myShop.model;

public class Consumable extends Item {
	
	private int quantity;
	
	public Consumable(String name, int id, float price, int nbrElt, int quantity) {
		super(name, id, price, nbrElt);
		this.quantity = quantity;
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the quantity
	 */	
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	

}
