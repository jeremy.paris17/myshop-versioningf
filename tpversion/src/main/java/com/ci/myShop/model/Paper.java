package com.ci.myShop.model;

public class Paper extends Consumable {
	

	private String quality;
	private float weight;
	
	public Paper(String name, int id, float price, int nbrElt, int quantity, String quality, float weight) {
		super(name, id, price, nbrElt, quantity);
		this.setQuality(quality);
		this.setWeight(weight);
		
		// TODO Auto-generated constructor stub
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}
	
	
	
}