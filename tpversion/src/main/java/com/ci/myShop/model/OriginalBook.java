package com.ci.myShop.model;

public class OriginalBook extends Book {

	boolean isNumerique ;

	public OriginalBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
			int year, int age, boolean isNumerique ) {
		super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
		this.isNumerique = isNumerique; 
		// TODO Auto-generated constructor stub
	}



	public boolean isNumerique() {
		return isNumerique;
	}

	public void setNumerique(boolean isNumerique) {
		this.isNumerique = isNumerique;
	}

}
