package com.ci.myShop.model;

public class Item {
	
	//Attributs
    String name;
    int id;
    float price;
    int nbrElt;
    
    public Item (String name, int id, float price, int nbrElt) {
        this.name = name;
        this.id = id;
        this.price = price ;
        this.nbrElt =nbrElt;
    }
    
    //Fonctions : ensemble des accesseurs    
    public String getName() {
        return name;
    }
        
    public int getId() {
        return id;
    }
    public float getPrice() {
        return price;
    }
    public int getNbrElt() {
        return nbrElt;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setPrice(float price) {
        this.price = price;
    }
    public void setNbrElt(int nbrElt) {
        this.nbrElt = nbrElt;
    }
        //Fonctions
        //permet de mettre en forme l'objet dans une String
        public String display() {
        return "name : " + name + " /id : " + id + " /price : " + price + " /nbrElt : " + nbrElt ;
    }
        

}
