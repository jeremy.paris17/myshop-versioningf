package com.ci.myShop.model;

public class Pen extends Consumable {

	private String colorString;
	private int durability;

		
	public Pen(String name, int id, float price, int nbrElt, int quantity, String colorString, int durability) {
		super(name, id, price, nbrElt, quantity);
		this.colorString = colorString;
		this.durability = durability;
		
				
		// TODO Auto-generated constructor stub
	}

	
	public String getColorString() {
		return colorString;
	}
	public void setColorString(String colorString) {
		this.colorString = colorString;
	}
	public int getDurability() {
		return durability;
	}
	public void setDurability(int durability) {
		this.durability = durability;
	}
	
	
	

}

