package com.ci.myShop.controller;

import java.util.ArrayList;
import java.util.List;

import com.ci.myShop.model.Book;
import com.ci.myShop.model.Consumable;
import com.ci.myShop.model.Item;
import com.ci.myShop.controller.Storage;;

public class Shop {
	// Attributs
   public static List<String> storage;
   public static float cash;
   
   public Shop (List<String> list,float cash1) {
       storage = list;
       cash = cash1;
   }

//Fonctions
    // permet de vendre un item s'il existe
   public static void sell(String Name){
       if (storage.contains(Name)) {
       Storage.getItem(Name).setNbrElt(Storage.getItem(Name).getNbrElt()-1);
       setCash(cash = cash + Storage.getItem(Name).getPrice());
   	}
   } 
   
   public static void setCash(float cash) {
       Shop.cash = cash;
   }
   
	public boolean buy(Item name) {
		if (name.getPrice()> Shop.cash) 
			return false;
		else {
			setCash(cash = cash-name.getPrice());
			return true;
		}	
		

	}

	public boolean isItemAvailable (String name) {
	       System.out.println("Nombre d'éléments disponibles : " + Storage.getItem(name).getNbrElt());
	       if (Storage.getItem(name).getNbrElt() > 0)
	           return true;
	       else
	           return false;
	   }
	public int getAgeForBook(String name) {
	       if (Storage.getItem(name) instanceof Book) {
	           return ((Book) Storage.getItem(name)).getAge();
	       }
	       else
	           return -1;
	   }
	public ArrayList<Book> getAllBook() {
	       ArrayList<Book> result = new ArrayList<Book>();
	       for (String  name : Storage.itemMap.keySet()){
	           if (Storage.itemMap.get(name) instanceof Book)
	               result.add((Book) Storage.getItem(name));
	       }
	       return result;
	   }
	public int getNbItemInStorage(String name) {
	       return Storage.getItem(name).getNbrElt();
	   }
	public int getQuantityPerConsumable(Consumable name) {
	       if (name instanceof Consumable) {
	           return name.getQuantity();
	       }
	       else
	           return -1;
	   }	
	
	

}

