
package com.ci.myShop;

import java.util.List;
import java.util.ArrayList;

import com.ci.myShop.controller.Shop;
import com.ci.myShop.controller.Storage;
import com.ci.myShop.model.Book;
import com.ci.myShop.model.Consumable;
import com.ci.myShop.model.Item;

public class Launch {

	public static void main(String[] args) {
		//créer une liste d'item
		List<String> list = new ArrayList<String>();
		
		//Crée deux items 
		Item item1 = new Item("livre", 1, 8, 3);
        Item item2 = new Item("stylo", 2, 1, 7);
	    Item ouvrage = new Item("Rousseau",42, 26, 4);
		Item montre = new Item("TimeLess",1, 29, 1);
		Book book = new Book("Jacques aux champis", 3, 18, 1, 212, "S.", "Lafont", 2000, 16);
		Book book1 = new Book("Java pour les nulls", 4, 29, 1, 313, "J.", "Marvel", 1990, 12);
		Consumable cons = new Consumable("Mine", 5, 3, 1, 25);
		
		
        //les rentre dans le storage
        Storage.addItem(item1);
        Storage.addItem(item2);
	    Storage.addItem(ouvrage);
	    Storage.addItem(book);
	    Storage.addItem(book1);
	    Storage.addItem(cons);
	    
        //les associe à la liste d'item
        list.addAll(Storage.itemMap.keySet());
        System.out.println(list);       
        // crée un shop initialisé avec la list
        Shop shop = new Shop (list, 100);
        
        //execute la fonction sell du Shop avec le nom d'un item existant
        Shop.sell("book");
        
        //affiche le cash du Shop
        System.out.println(Shop.cash);
        
        //execute la fonction buy du Shop avec le nom d'un item existant
	    System.out.println("Achat d'Item possible ? " + shop.buy(montre));
	    
	    //affiche le cash du Shop
		System.out.println("Cash : " + Shop.cash);
				
		System.out.println(shop.isItemAvailable("Mine")); 
		
		System.out.println(shop.getAgeForBook("Java pour les nulls"));
		   
		System.out.println(shop.getAllBook());
	
		System.out.println(shop.getNbItemInStorage("stylo"));
		

	
		
		

	}
		
}

	
